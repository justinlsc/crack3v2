#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

struct entry 
{
    char *hash;
    char *word;
};

int bcompare(const void *t, const void *a)
{
  const char *key = t;
  const char * const *arg = a;
  return strcmp(key, *arg);
}


int compare(const void *a, const void *b)
{
    // Make a copy of a and b
    char aa[HASH_LEN], bb[HASH_LEN];
    strcpy(aa, *(char **)a);
    strcpy(bb, *(char **)b);
    
    // Convert aa and bb to lowercase
    for (int i = 0; i < strlen(aa); i++)
        aa[i] = tolower(aa[i]);
    for (int i = 0; i < strlen(bb); i++)
        bb[i] = tolower(bb[i]);
    
    // Compare aa to bb using strcmp
    return strcmp(aa, bb);
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Compare the two hashes
    if(strcmp(hash, guess) == 0) return 1;
    return 0;
}

int file_length(char *filename)
{
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
        return -1;
    else
        return fileinfo.st_size;
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size, char **file)
{
    //Obtain length of file
    int len = file_length(filename);
    //printf("%d\n",len);
    if (len == -1)
    {
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    //Allocate memory for the entire file
    char *file_contents = malloc(len);
    
    //Read entire file into file contents
    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    //Replace \n with \0.
    //Also keep count as we go.
    int line_count =0;
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    //Allocate array of pointers to each line
    char **lines = malloc(line_count * sizeof(char *));
    
    //Fill in each entry with address of corresponding line
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        
        //Scan forward to find next line
        while (file_contents[c] != '\0') c++;
        c++;
    }
    
    *size = line_count;
    struct entry *x;
    x = malloc(line_count * sizeof(struct entry));
    
    for(int i = 0; i < line_count; i++ )
    {
        //Filling struct with hash and password
        x[i].word=lines[i];
        x[i].hash=md5(lines[i], strlen(lines[i]));
    }
    
    free(lines);
    //free(file_contents); //causing errors in valgrind
    
    //printf("\n\n");
    //printf("%p  %p\n",file, file_contents);
    *file = (file_contents);
    //printf("%p  %p\n",*file, file_contents);
    
    return x;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    clock_t begin = clock();

    int dlen;
    char *file;
    struct entry *dict = read_dictionary(argv[2], &dlen, &file);
    qsort(dict, dlen, sizeof(struct entry), compare);
    
    // Open the hash file for reading.
    //Read entire file into file contents
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    char line[PASS_LEN];
    int count =0;
    while(fgets(line, 100, fp) != NULL)
    {
        char *pos;
        if ((pos=strchr(line, '\n')) != NULL)
            *pos = '\0';
            
        struct entry *found = bsearch(line, dict, dlen, sizeof(struct entry), bcompare);
        //printf("%s\n", found);
        if (found != NULL)
        {
            count++;
            printf("%d) %s matches with %s\n",count, found->word,line); //causing errors in valgrind
        }
    }
    
    fclose(fp);
    for(int i = 0; i < dlen; i++ )
    {
        free(dict[i].hash);
        //free(dict[i].word);
    }
    //free(dict->hash);
    //free(dict[0].hash);
    free(dict);
    free(file);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("\nExecution Time: %f\n", time_spent);
}
